<?php
/**
 * 
 */
class Realisasi extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		if(!$this->session->has_userdata('username') || $this->session->username==null){
			redirect('Login');
		}
		$this->load->model('Realisasi_model');
	}

	public function index()
	{
		$page = 'realisasi';
		if($this->session->user_level!="admin"):
		$data = array('page'=>$page, 'data_uraian'=>$this->Realisasi_model->get_uraian_to_input($this->session->username), 'data_realisasi'=>$this->Realisasi_model->get_realisasi($this->session->username));
		else:
			$data = array('page'=>$page, 'data_uraian'=>$this->Realisasi_model->get_uraian_to_input($this->session->username), 'data_realisasi'=>$this->Realisasi_model->get_realisasi());
		endif;
		$this->load->view($page.'_view', $data);
	}

	public function insert_realisasi()
	{
		$kode_rekening = $this->input->post('jenis_uraian');
		$anggaran_tahun = $this->input->post('angg_tahun');
		$anggaran_bulan = $this->input->post('angg_bulan');
		$sub1 = $this->input->post('sub1');
		$sub2 = $this->input->post('sub2');
		$sub3 = $this->input->post('sub3');

		$get_nomor_sub = $this->db->get_where('sub_uraian', array('kode_rekening'=>$kode_rekening, 'sub' => $sub1, 'subs' => $sub2, 'subss' => $sub3))->row()->no;

		$jumlah = $this->input->post('jumlah');
		$realisasi = $this->input->post('realisasi');
		$data = array('kode_rekening'=>$kode_rekening, 'anggaran_tahun'=>$anggaran_tahun, 'anggaran_bulan'=>$anggaran_bulan, 'jumlah'=>$jumlah, 'realisasi'=>$realisasi, 'no_sub' => $get_nomor_sub);
		if($this->Realisasi_model->add_realisasi($data)){
			return true;
		}else{
			return false;
		}

	}

	public function get_input_form1()
	{
		$kode_rekening = $this->input->post("kode_rekening");
		$array_where = array('kode_rekening' => $kode_rekening);
		$data_GET = $this->Realisasi_model->get_form_sub1_where($array_where);
		// var_dump($data_GET);
		$select = '<select name="sub1" id="sub1" class="form-control">';
		$select .= '<option selected disabled></option>';
		
			foreach ($data_GET as $row) {
				$select .= '<option value="'.$row->sub.'" label="'.$row->nama_sub.'">'.$row->nama_sub.'</option>';
			}
		$select .= '</select>';
		echo $select;
	}

	public function get_input_form2()
	{
		$kode_rekening = $this->input->post("kode_rekening");
		$sub = $this->input->post("sub");
		$array_where = array('kode_rekening' => $kode_rekening, 'sub' => $sub);
		$data_GET = $this->Realisasi_model->get_form_sub2_where($array_where);
		$select = '<select name="sub2" id="sub2" class="form-control">';
		$select .= '<option selected disabled></option>';
		
			foreach ($data_GET as $row) {
				$select .= '<option value="'.$row->subs.'" label="'.$row->nama_sub.'">'.$row->nama_sub.'</option>';
			}
		$select .= '</select>';
		echo $select;
	}

	public function get_input_form3()
	{
		$kode_rekening = $this->input->post("kode_rekening");
		$sub = $this->input->post("sub");
		$subs = $this->input->post("subs");
		$array_where = array('kode_rekening' => $kode_rekening, 'sub' => $sub, 'subs' => $subs);
		$data_GET = $this->Realisasi_model->get_form_sub3_where($array_where);
		$select = '<select name="sub3" id="sub3" class="form-control">';
		// $select .= '<option selected disabled></option>';
		
			foreach ($data_GET as $row) {
				$select .= '<option value="'.$row->subss.'" label="'.$row->nama_sub.'" data-id = "'.$row->no.'">'.$row->nama_sub.'</option>';
			}
		$select .= '</select>';
		echo $select;
	}

	public function get_max_harga()
	{
		header("Content-type:Application/json");
		$kode_rekening = $this->input->get("kode_rekening");
		$sub = $this->input->get("sub");
		$subs = $this->input->get("subs");
		$subss = $this->input->get('subss');
		$array_where = array('kode_rekening' => $kode_rekening, 'sub' => $sub, 'subs' => $subs, 'subss' => $subss);

		$harga_max = $this->db->get_where('sub_uraian', $array_where)->row();
		
		echo json_encode($harga_max);
	}


}