<?php
/**
 * 
 */
class ManageUsers extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		if(!$this->session->has_userdata('username') || $this->session->username==null){
			redirect('Login');
		}elseif($this->session->username!="admin"){
			exit("Anda Tidak dapat Mengakses halaman");
		}
		$this->load->model('ManageUsers_model');
	}

	public function index()
	{
		$page = "manageUsers";
		$data = array('page'=>$page, 'data_users'=>$this->ManageUsers_model->select_users()->result());
		
		$this->load->view($page."_view", $data);
	}
}