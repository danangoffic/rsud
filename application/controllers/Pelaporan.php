<?php
/**
 * 
 */
class Pelaporan extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		if(!$this->session->has_userdata('username') || $this->session->username==null){
			redirect('Login');
		}
		$this->load->model('Pelaporan_model');
	}

	public function index()
	{
		$page = 'pelaporan';
		$data = array('page'=>$page);
		$this->load->view($page.'_view', $data);
	}

	public function get_data_laporan()
	{
		$datas = $this->Pelaporan_model->get_data();
		$no = 1;
		var_dump($datas);
		foreach ($datas as $key) {
			echo "<tr id='".$key['kode_rekening']."'>
					<td class='text-center'>".$key['kode_rekening']."</td>
					<td>".$key['nama_buah']."</td>
					<td class='text-center'>
					<button data-toggle='modal' data-target='#modalEditMain' class='btn btn-success' id='edit_button' data-rek='".$key['kode_rekening']."'>Edit</button>
					<a class='btn btn-danger' href='".base_url('Pelaporan/delete_main_sub?no_rek=').$key['kode_rekening']."' id='delete_main_sub' data-rek='".$key['kode_rekening']."'>Delete</a>
					<button type='button' class='btn btn-info' data-toggle='modal' data-target='#modal_sub' data-rekening='".$key['kode_rekening']."'>Sub Uraian</button>
					
					</td>
					</tr>";
					$no++;
		}
	}

	public function add_main_sub()
	{
		$nama_uraian = $this->input->post('nama_uraian_main');
		$kode_rekening = $this->input->post('kode_rekening_main');
		return $this->db->insert('data_rka_laporan', array('kode_rekening'=>$kode_rekening, 'nama_buah'=>$nama_uraian, 'username'=>$this->session->username));
	}

	public function delete_main_sub()
	{
		$no_rek = $this->input->get('no_rek');
		$this->db->delete('sub_uraian', array('kode_rekening'=>$no_rek));
		$this->db->delete('data_rka_laporan', array('kode_rekening'=>$no_rek));
		redirect(base_url('Pelaporan'));
	}

	public function sub_uraian()
	{
		header("Content-type:application/json");
		$no_rek = $this->input->post('no_rekening');
		$datas = $this->Pelaporan_model->get_sub1($no_rek);
		$datat = $this->Pelaporan_model->get_title_sub1($no_rek);
		$data = array('title'=>$datat->nama_buah, 'no_rek'=>$no_rek,
					'records'=>$datas);
		echo json_encode($data);
	}

	public function add_sub1()
	{
		$no_rek = $this->input->post('no_rekening');
		$uraian_sub = $this->input->post('uraian_sub');
		$insertArray = array('kode_rekening'=>$no_rek, 'nama_sub'=>$uraian_sub);
		return $this->Pelaporan_model->add_sub1($insertArray);
		// $read_last_sub = $this->db->query('SELECT `sub` FROM sub_uraian WHERE kode_rekening = "$no_rek" ORDER BY sub DESC LIMIT 1')->row();
		// $array_input = array('kode_rekening'=>$no_rek, 'nama_sub'=>$uraian_sub, 'sub'=>$read_last_sub->sub+1);		
		// return $this->db->insert('sub_uraian', $array_input);
		
	}

	public function delete_sub1()
	{
		$no = $this->input->get('no');
		$this->db->delete('sub_uraian', array('no'=>$no));
		redirect(base_url('Pelaporan'));
	}

	public function sub_uraian2()
	{
		header("Content-type:application/json");
		$kode_rekening = $this->input->get('no_rekening');
		$sub = $this->input->get('sub');
		$titlemain = $this->Pelaporan_model->get_title_sub1($kode_rekening)->nama_buah;
		$titlesub = $this->Pelaporan_model->get_title_sub2($kode_rekening, $sub)->nama_sub;
		$datas = $this->Pelaporan_model->get_sub2($kode_rekening, $sub);
		$data = array('titlemain'=>$titlemain, 'titlesub'=>$titlesub, 'no_rekening'=> $kode_rekening, 'sub'=>$sub, 'records'=>$datas);
		echo json_encode($data);
	}

	public function add_sub2()
	{
		$no_rekening = $this->input->post('no_rekening');
		$uraian_sub = $this->input->post('uraian_sub');
		$id_sub = $this->input->post('id_sub');
		$insertArray = array('kode_rekening'=>$no_rekening, 'nama_sub'=>$uraian_sub, 'sub'=>$id_sub);
		$this->Pelaporan_model->add_sub2($insertArray);

	}

	public function sub_uraian3()
	{
		header("Content-type:application/json");
		$kode_rekening = $this->input->get('no_rekening');
		$sub = $this->input->get('sub');
		$subs = $this->input->get('subs');
		$titlemain = $this->Pelaporan_model->get_title_sub1($kode_rekening)->nama_buah;
		$titlesub = $this->Pelaporan_model->get_title_sub2($kode_rekening, $sub)->nama_sub;
		$titlesub2 = $this->Pelaporan_model->get_title_sub3($kode_rekening, $sub, $subs)->nama_sub;
		$datas = $this->Pelaporan_model->get_sub3($kode_rekening, $sub, $subs);
		$data = array('titlemain'=>$titlemain, 'titlesub'=>$titlesub, 'no_rekening'=> $kode_rekening, 'sub'=>$sub,'subs'=>$subs, 'records'=>$datas);
		echo json_encode($data);
	}

	public function add_sub3()
	{
		$no_rekening = $this->input->post('no_rekening');
		$uraian_sub = $this->input->post('uraian_sub');
		$sub = $this->input->post('sub');
		$subs = $this->input->post('subs');
		$volume = $this->input->post('volume');
		$satuan = $this->input->post('satuan_volume');
		$harga_satuan = $this->input->post('hargasatuan');
		$jumlah = $volume * $harga_satuan;
		$insertArray = array('kode_rekening'=>$no_rekening, 'nama_sub'=>$uraian_sub, 'sub'=>$sub, 'subs'=>$subs,'volume'=>$volume, 'harga_satuan'=>$harga_satuan, 'jumlah'=>$jumlah, 'satuan'=>$satuan);
		return $this->Pelaporan_model->add_sub3($insertArray);
	}

	public function getDataMain()
	{
		header("Content-type:application/json");
		$kode_rekening = $this->input->post('kode_rekening');
		$data = $this->db->query("SELECT * FROM data_rka_laporan WHERE kode_rekening = '$kode_rekening'")->row();
		// $data = $this->db->get_where('sub_uraian', array('kode_rekening'=>$kode_rekening, 'sub IS NOT NULL', 'subs IS NULL', 'subss IS NULL'))->row();
		echo json_encode($data);
	}

	public function updateMain()
	{
		$kr = $this->input->post('edit_main_kd');
		$kode_rekening = $this->input->post('edit_kode_rekening_main');
		$nama_sub = $this->input->post('edit_nama_uraian_main');
		$data = array('nama_buah'=>$nama_sub);
		$query = $this->db->query("UPDATE `rsud`.`data_rka_laporan` SET `nama_buah` = '$nama_sub', kode_rekening = '$kode_rekening' WHERE `data_rka_laporan`.`kode_rekening` = '$kr'");
		
		if($query==true):
			echo "true";
		else:
			echo "false";
		endif;
	}

	public function getDataSub()
	{
		header("Content-type:application/json");
		$no = $this->input->post('no');
		$query = $this->db->get_where('sub_uraian', array('no'=>$no));
		echo json_encode($query->row());
	}

	public function updateDataSub()
	{
		$no = $this->input->post('no1');
		$nama_sub = $this->input->post('edit_uraian_sub1');
		$data = array('nama_sub'=>$nama_sub);
		if($this->db->update('sub_uraian', $data, array('no'=>$no))):
			echo "true";
		else:
			echo "false";
		endif;
	}

	public function getDataSub2()
	{
		
	}

	public function updateDataSub2()
	{
		$no = $this->input->post('no2');
		$nama_sub = $this->input->post('edit_uraian_sub2');
		$data = array('nama_sub'=>$nama_sub);
		return $this->db->update('sub_uraian', $data, array('no'=>$no));
	}

	public function getDataSub3()
	{
		# code...
	}

	public function updateDataSub3()
	{
		$no = $this->input->post('no3');
		$nama_sub = $this->input->post('uraian_sub3');
		$volume = $this->input->post('volume3');
		$satuan = $this->input->post('edit_satuan_volume');
		$harga_satuan = $this->input->post('hargasatuan3');
		$jumlah = $volume * $harga_satuan;
		$data = array('no'=>$no, 'nama_sub' => $nama_sub, 'volume' => $volume, 'harga_satuan' => $harga_satuan, 'jumlah' => $jumlah, 'satuan' => $satuan);
		var_dump($data);
		return $this->db->update('sub_uraian', $data, array('no' => $no));
	}
}