<?php
/**
 * 
 */
class Rka extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		if(!$this->session->has_userdata('username') || $this->session->username==null){
			redirect('Login');
		}
		$this->load->model('Rka_model');
	}

	public function index()
	{
		$page = 'rka';
		$data = array('page'=>$page);
		$this->load->view($page.'_view', $data);
	}

	public function get_data()
	{
		$datas = $this->Rka_model->get_data();
		foreach ($datas as $key) {
			echo "<tr>
				<td class='text-center'>".$key['no_rek']."</td>
				<td>".$key['uraian']."</td>
				<td>".$key['hargasatuan']."</td>
				<td>".$key['jumlah']."</td>
				
				</tr>";
		}
	}

	public function adding_data()
	{
		$kode_rekening = $this->input->post('kode_rekening');
		$uraian = $this->input->post('uraian');
		$hargasatuan = $this->input->post('hargasatuan');
		$jumlah = $this->input->post('jumlah');
		$data_insert = array('no_rek'=>$kode_rekening,'uraian'=>$uraian,'hargasatuan'=>$hargasatuan,'jumlah'=>$jumlah);
		if($this->Rka_model->add_data($data_insert)){
			return true;
		}else{
			return false;
		}

	}
}