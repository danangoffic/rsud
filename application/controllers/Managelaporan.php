<?php
/**
 * 
 */
class Managelaporan extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		if(!$this->session->has_userdata('username') || $this->session->username==null){
			redirect('Login');
		}
		$this->load->model("Managelaporan_model");
		$this->page = 'managelaporan';
	}

	public function index()
	{

		$data = array('page'=>$this->page, 'all_rka'=>$this->Managelaporan_model->get_RKA(),'all_pptk'=>$this->Managelaporan_model->get_PPTK());
		$this->load->view('managelaporan_view', $data);
	}

	public function get_excel_data()
	{
		$rka = $this->input->post('rka');
		$pptk = $this->input->post('pptk');
		$data_rka = $this->db->get_where('data_rka_laporan', array('kode_rekening'=>$rka))->row();
		$query = $this->db->query("SELECT data_rka_laporan.kode_rekening, data_rka_laporan.nama_buah, 
				sub_uraian.nama_sub, sub_uraian.sub, sub_uraian.subs, sub_uraian.subss, sub_uraian.volume, sub_uraian.satuan, sub_uraian.harga_satuan, sub_uraian.jumlah AS jumlah_harga,
				`data_realisasi`.`anggaran_tahun`, `data_realisasi`.`anggaran_bulan`, `data_realisasi`.`jumlah` AS jumlah_realisasi, data_realisasi.realisasi,
				`users`.username
				 FROM data_rka_laporan 
			JOIN sub_uraian 
			ON sub_uraian.kode_rekening LIKE data_rka_laporan.kode_rekening
			JOIN users 
			ON users.username LIKE  data_rka_laporan.username
			JOIN data_realisasi 
			ON data_realisasi.kode_rekening LIKE data_rka_laporan.kode_rekening
			WHERE data_rka_laporan.kode_rekening = '$rka' AND data_rka_laporan.username = '$pptk'");
		$max = $this->db->query("SELECT sum(sub_uraian.jumlah) as total_di_awal
						FROM data_rka_laporan 
						JOIN sub_uraian 
						ON sub_uraian.kode_rekening LIKE data_rka_laporan.kode_rekening
						JOIN users 
						ON users.username LIKE  data_rka_laporan.username
						JOIN data_realisasi 
						ON data_realisasi.kode_rekening LIKE data_rka_laporan.kode_rekening
						WHERE data_rka_laporan.kode_rekening = '$rka' AND data_rka_laporan.username = '$pptk'")->row();
		?>
		<table class="table">
			<tr>
				<td>Nama PPTK:</td>
				<td><?=$data_rka->username;?></td>
			</tr>
			<tr>
				<td>Uraian Utama:</td>
				<td><?=$data_rka->data_buah;?></td>
			</tr>
		</table>
		<table class="table table-condensed table-bordered table-hover table-striped">
			<thead>
				<tr>
					<th rowspan="2">Kode Rekening</th>
					<th rowspan="2">Uraian</th>
					<th colspan="2">Rincian Penghitungan</th>
					<th rowspan="2">Jumlah</th>
					<th colspan="4">Arus Khas Triwulan</th>
				</tr>
				<tr>
					<th>Volume</th>
					<th>Satuan VOlume</th>
					<th>Harga Satuan</th>
					<th>1</th>
					<th>2</th>
					<th>3</th>
					<th>4</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><?=$data_rka->kode_rekening;?></td>
					<td><?=$data_rka->nama_buah;?></td>

				</tr>
			</tbody>
		</table>
		<?php
	}
}