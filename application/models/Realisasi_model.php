<?php

/**
 * 
 */
class Realisasi_model extends CI_Model
{
	
	public function get_uraian_to_input($u)
	{
		if($u!=""):			
			$query = $this->db->query("SELECT * FROM `data_rka_laporan` WHERE username = '$u'");
		else:
			$query = $this->db->query("SELECT * FROM `data_rka_laporan`");
		endif;
		return $query->result();
	}

	public function get_realisasi($u=null)
	{
		if($u!=null):
			$query = $this->db->query("SELECT data_realisasi.kode_rekening, data_rka_laporan.nama_buah as uraian, `anggaran_tahun`, `anggaran_bulan`, FORMAT(`jumlah`, 2) AS jumlah, `realisasi` FROM `data_realisasi` JOIN data_rka_laporan ON data_rka_laporan.kode_rekening = data_realisasi.kode_rekening WHERE data_rka_laporan.username = '$u'");
		else:
			$query = $this->db->query("SELECT data_realisasi.kode_rekening, data_rka_laporan.nama_buah as uraian, `anggaran_tahun`, `anggaran_bulan`, FORMAT(`jumlah`, 2) AS jumlah, `realisasi` FROM `data_realisasi` JOIN data_rka_laporan ON data_rka_laporan.kode_rekening = data_realisasi.kode_rekening");
		endif;
		return $query->result();
	}

	public function add_realisasi($data)
	{
		$query = $this->db->insert('data_realisasi', $data);
		return $query;
	}

	public function delete_ralisasi($where)
	{
		# code...
	}

	public function get_sum1($rek)
	{
		$query = $this->db->query("SELECT SUM(jumlah) AS total FROM sub_uraian WHERE kode_rekening = '$rek' AND jumlah IS NOT NULL AND sub IS NOT NULL AND subs IS NOT NULL AND subss IS NOT NULL");
		return $query->row();
	}

	public function get_form_sub1_where($data)
	{
		$kode_rekening = $data['kode_rekening'];
		$query = $this->db->query("SELECT * FROM sub_uraian WHERE kode_rekening = '$kode_rekening' AND sub IS NOT NULL AND subs IS NULL AND subss IS NULL");
		return $query->result();
	}

	public function get_form_sub2_where($data)
	{
		$kode_rekening = $data['kode_rekening'];
		$sub = $data['sub'];
		$query = $this->db->query("SELECT * FROM sub_uraian WHERE kode_rekening = '$kode_rekening' AND sub = '$sub' AND subs IS NOT NULL AND subss IS NULL");
		return $query->result();
	}

	public function get_form_sub3_where($data)
	{
		$kode_rekening = $data['kode_rekening'];
		$sub = $data['sub'];
		$subs = $data['subs'];
		$query = $this->db->query("SELECT * FROM sub_uraian WHERE kode_rekening = '$kode_rekening' AND sub = '$sub' AND subs ='$subs' AND subss IS NOT NULL");
		return $query->result();
	}

}