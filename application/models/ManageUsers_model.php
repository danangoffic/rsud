<?php
/**
 * 
 */
class ManageUsers_model extends CI_Model
{
	
	public function select_users()
	{
		$query = $this->db->query("SELECT * FROM users");
		return $query;
	}

	public function insert_user($data)
	{
		$query = $this->db->insert('users', $data);
		return $query;
	}

	public function update_user($data, $where)
	{
		$query = $this->db->update('users', $data, $where);
		return $query;
	}

	public function delete_user($where)
	{
		$query = $this->db->delete('users', $where);
		return $query;
	}

}