<?php
/**
 * 
 */
class Pelaporan_model extends CI_Model
{

		// GET DATA
	public function get_data()
	{
		$query = $this->db->query("SELECT * FROM data_rka_laporan ORDER BY nama_buah ASC");
		return $query->result_array();
	}

	// GET SUB 1
	public function get_sub1($no_rek)
	{
		$query = $this->db->query("SELECT `no`, `kode_rekening`, `nama_sub`, `sub`, `subs`, `harga_satuan`, `jumlah` FROM `sub_uraian` WHERE kode_rekening = '$no_rek' AND `sub` IS NOT NULL AND `subs` IS NULL ORDER BY sub ASC");
		return $query->result();
	}

	// GET TITLE SUB 1
	public function get_title_sub1($no_rek)
	{
		$query = $this->db->query("SELECT nama_buah FROM `data_rka_laporan` WHERE `kode_rekening` = '$no_rek'");
		return $query->row();
	}

	// ADD SUB 1
	public function add_sub1($data)
	{
		$rek = $data['kode_rekening'];
		$nama_sub = $data['nama_sub'];
		$last_num = $this->cek_sub1($rek)->sub;
		if($last_num=="" || $last_num == null):
			$last_num=1;
		else:
			$last_num = $last_num+1;
		endif;
		// $data2 = array('sub'=>$last_num);
		// $merge = array_merge($data,$data2);
		// echo($last_num);
		// return $this->db->insert('sub_uraian', $merge);
		echo($last_num);
		var_dump($this->db->query("INSERT INTO `sub_uraian` SET `kode_rekening` = '$rek', `nama_sub` = '$nama_sub', `sub` = '$last_num'"));
		
	}

	// CEK TOTAL SUB 1
	public function cek_sub1($rek)
	{
		$query = $this->db->query("SELECT MAX(sub) as sub FROM sub_uraian WHERE kode_rekening = '$rek'");
		return $query->row();
	}

	// GET SUB 2
	public function get_sub2($no_rek, $sub)
	{
		$query = $this->db->query("SELECT `no`, `kode_rekening`, `nama_sub`, `sub`, `subs`, `harga_satuan`, `jumlah` FROM `sub_uraian` WHERE `kode_rekening` = '$no_rek' AND `sub` = '$sub' AND `subs` IS NOT NULL AND subss IS NULL ORDER BY subs ASC");
		return $query->result();
	}

	//GET TITLE SUB 2
	public function get_title_sub2($no_rek, $sub)
	{
		$query = $this->db->query("SELECT `nama_sub` FROM `sub_uraian` WHERE kode_rekening = '$no_rek' AND `sub` ='$sub' AND `subs` IS NULL");
		return $query->row();
	}

	public function add_sub2($data)
	{
		$no_rek = $data['kode_rekening'];
		$sub = $data['sub'];
		$last_num = $this->cek_sub2($no_rek, $sub)->subs;
		$data2  = array('subs'=>$last_num+1);
		$array_merge = array_merge($data, $data2);
		$query = $this->db->insert('sub_uraian', $array_merge);
	}

	public function cek_sub2($no_rek, $sub)
	{
		$query = $this->db->query("SELECT MAX(subs) as subs FROM sub_uraian WHERE kode_rekening = '$no_rek' AND sub = '$sub'");
		return $query->row();
	}

	public function get_sub3($no_rek, $sub, $subs)
	{
		$query = $this->db->query("SELECT `no`, `kode_rekening`, `nama_sub`, `sub`, `subs`,`subss`, volume, FORMAT(`harga_satuan`, 0) AS harga_satuan, FORMAT(`jumlah`, 0) AS jumlah, 'Rp ' as rp FROM `sub_uraian` WHERE `kode_rekening` = '$no_rek' AND `sub` = '$sub' AND `subs` = '$subs' AND subss IS NOT NULL ORDER BY subs ASC");
		return $query->result();
	}

	public function get_title_sub3($no_rek, $sub, $subs)
	{
		$query = $this->db->query("SELECT `nama_sub` FROM `sub_uraian` WHERE kode_rekening = '$no_rek' AND `sub` ='$sub' AND `subs` = '$subs'");
		return $query->row();
	}

	public function add_sub3($data)
	{
		$no_rek = $data['kode_rekening'];
		$sub = $data['sub'];
		$subs = $data['subs'];
		$last_num = $this->cek_sub3($no_rek, $sub, $sub)->subss;
		echo($last_num);
		$data2 = array('subss'=>$last_num+1);
		$array_merge = array_merge($data, $data2);
		$query = $this->db->insert('sub_uraian', $array_merge);
	}

	public function cek_sub3($no_rek, $sub, $subs)
	{
		$query = $this->db->query("SELECT MAX(subss) as subss FROM sub_uraian WHERE kode_rekening = '$no_rek' AND sub = '$sub' AND subs = '$subs'");
		return $query->row();
	}


}