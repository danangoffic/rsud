<!DOCTYPE html>
<html lang="en">
	<?php $this->load->view('templates/head'); ?>
<body>
    <div id="wrapper">
		<!-- NAVIGATION -->
		<?php $this->load->view('templates/nav'); ?>
		<!-- END NAVIGATION -->

		<!-- CONTENT -->
         <div id="page-wrapper" style="min-height: 650px">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-home"></i> Dashboard
                            </li>
                            <li class="active">
                                <i class="fa fa-newspaper"></i> Data Laporan
                            </li>
                        </ol>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-8">
                        <form action="" name="form-find-laporan">
                            <div class="form-group col-lg-12 has-feedback" id="feedback-rka">
                                <label class="col-lg-4">Pilih RKA:</label>
                                <div class="col-lg-8">
                                    <select class="form-control" name="rka" id="rka" autofocus="">
                                        <option disabled="" label=""></option>
                                        <?php
                                        foreach($all_rka as $rowrka){
                                        ?>
                                        <option value="<?=$rowrka->kode_rekening;?>" label="<?=$rowrka->nama_buah;?>"><?=$rowrka->nama_buah;?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-lg-12 has-feedback" id="feedback-pptk">
                                <label class="col-lg-4">Pilih PPTK:</label>
                                <div class="col-lg-8">
                                    <select class="form-control" name="pptk" id="pptk">
                                        <option disabled="" label=""></option>
                                        <?php
                                        foreach($all_pptk as $rowpptk){
                                        ?>
                                        <option value="<?=$rowpptk->username;?>" label="<?=$rowpptk->username;?>"><?=$rowpptk->username;?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-lg-12">
                                
                                    <button class="btn btn-primary pull-right" type="button" id="submit-cari"><i class="fa fa-fw fa-search"></i> Cari</button>
                                
                            </div>
                        </form>
                    </div>
                </div>

                <div class="row" id="list-hasil">
                    
                </div>
            </div>
        </div>
    
    <!-- /#page-wrapper -->
		<!-- END CONTENT -->

	</div>
		<!-- JS SCRIPT -->
		<?php $this->load->view('templates/script'); ?>
		<script type="text/javascript">
            var rka = $("#rka").val();
            var pptk = $("#pptk").val();
        $("#rka").change(function () {
            if($(this).val() != null || !empty($(this).val())){
                $("#feedback-rka").removeClass('has-warning');
                $("#submit-cari").removeClass('disabled');
            }else{
                $("#feedback-rka").addClass('has-warning');
                $("#submit-cari").addClass('disabled');
            }
        });
        $("#pptk").change(function(){
            if($(this).val() != null || !empty($(this).val())){
                $("#feedback-pptk").removeClass('has-warning');
                $("#submit-cari").removeClass('disabled');
            }else{
                $("#feedback-pptk").addClass('has-warning');
                $("#submit-cari").addClass('disabled');
            }
        });
        $("#submit-cari").click(function(){
            if((rka==""||rka==null)||(pptk==""||pptk==null)){
                alert("Pilih RKA dan PPTK terlebih dahulu");
            }else{
                $.post("<?=base_url('Managelaporan/get_excel_data');?>", $("#form-find-laporan").serialize(), function(hasil){
                    $("#list-hasil").html(hasil);

                });
            }
        });
        </script>
		<!-- END JS SCRIPT -->	
</body>
</html>