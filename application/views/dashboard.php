<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('templates/head'); ?>
<body>
    <div id="wrapper">
        <!-- NAVIGATION -->
        <?php $this->load->view('templates/nav'); ?>
        <!-- END NAVIGATION -->
        <!-- CONTENT -->
         <div id="page-wrapper" style="min-height: 650px">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Dashboard <small>&nbsp;</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-home"></i> Dashboard
                            </li>
                        </ol>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <h2 class="text-center">SELAMAT DATANG DI SISTEM DAN APLIKASI PELAPORAN RSUD PREMBUN</h2>
                    </div>

            </div>
            <!-- /.container-fluid -->
            <!-- /.CONTENT -->

        </div>
        <!-- /#page-wrapper -->
        <!-- END CONTENT -->


    </div>
        <!-- JS SCRIPT -->
        <?php $this->load->view('templates/script'); ?>
        <!-- END JS SCRIPT -->  
</body>
</html>