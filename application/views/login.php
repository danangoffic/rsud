<?php
require 'config.php';
$page = 'Login';
?>
<!DOCTYPE html>
<html>
	<?php $this->load->view('templates/head'); ?>
<body>
	
		<!-- CONTENT LOGIN -->
		<div class="container">
	        <div class="row">
	            <div class="col-md-6 col-md-offset-3">
	                <div class="login-panel panel panel-default">
	                    <div class="panel-heading text-center">
	                        <img class="img-rounded" src="<?=base_url('library/kebumen.jpg')?>" alt="" width="150">
            				<h3>SISTEM DAN APLIKASI PELAPORAN<br> RSUD PREMBUN<br>KABUPATEN KEBUMEN</h3>
            				<h3 class="panel-title">Please Sign In</h3>
	                    </div>
	                    <div class="panel-body">
	                        <form class="form-horizontal" role="form" action="Login/cek_log" method="POST">
	                            <fieldset>
	                                <div class="form-group">
	                                	<div class="col-sm-12 col-md-offset-0">
	                                    <input class="form-control" placeholder="Username" name="username" type="text" autofocus="">
	                                    </div>
	                                </div>
	                                <div class="form-group">
	                                	<div class="col-sm-12 col-md-offset-0">
	                                    <input class="form-control" placeholder="Password" name="password" type="password" value="">
	                                    </div>
	                                </div>
	                                <!-- <div class="checkbox">
	                                    <label>
	                                        <input name="remember" type="checkbox" value="Remember Me">Remember Me
	                                    </label>
	                                </div> -->
	                                
	                                <input type="submit" name="" value="LOGIN" class="btn btn-success btn-block">
	                            </fieldset>
	                        </form>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	    <!-- END CONTENT LOGIN -->

		<!-- JS SCRIPT -->
		<?php $this->load->view('templates/script.php'); ?>
		<!-- END JS SCRIPT -->
	
</body>
</html>