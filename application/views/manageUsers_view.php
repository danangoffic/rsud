<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('templates/head'); ?>
<body>
    <div id="wrapper">
        <!-- NAVIGATION -->
        <?php $this->load->view('templates/nav'); ?>
        <!-- END NAVIGATION -->
        <!-- CONTENT -->
         <div id="page-wrapper" style="min-height: 650px">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Manage Users <small>&nbsp;</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-home"></i> Dashboard
                            </li>
                            <li class="active">
                                <i class="fa fa-users-cog"></i> Manage Users
                            </li>
                        </ol>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12">
                    <?php 
                    $no = 1;
                    if(isset($data_users)): ?>
                    <table class="table table-condensed table-bordered table-hover table-striped">
                        <thead>
                            <tr>
                                <th class="text-center">No</th>
                                <th class="text-center">Username</th>
                                <th class="text-center">ID User as</th>
                                <th class="text-center"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($data_users as $key) {
                                ?>
                                <tr>
                                    <td class="text-center"><?=$no;?></td>
                                    <td class="text-left"><strong><u><?=$key->username;?></u></strong></td>
                                    <td class="text-center"><i><?=$key->id_users;?></i></td>
                                    <td class="text-center">
                                        <a class="btn btn-success" href="" target="_self"><i class="fa fa-fw fa-edit"></i> Edit</a>
                                    </td>
                                </tr>
                                <?php
                                $no++;
                            }
                            ?>
                        </tbody>
                    </table>
                <?php else:
                    echo "Data Kosong"; 
                    endif;?>
                    </div>
                </div>
            <!-- /.container-fluid -->
            <!-- /.CONTENT -->

        </div>
        <!-- /#page-wrapper -->
        <!-- END CONTENT -->


    </div>
        <!-- JS SCRIPT -->
        <?php $this->load->view('templates/script'); ?>
        <!-- END JS SCRIPT -->  
</body>
</html>