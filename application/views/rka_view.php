<!DOCTYPE html>
<html lang="en">
	<?php $this->load->view('templates/head'); ?>
<body>
    <div id="wrapper">
		<!-- NAVIGATION -->
		<?php $this->load->view('templates/nav'); ?>
		<!-- END NAVIGATION -->
        <!-- START CONTENT RKA VIEW -->

        <!-- CONTENT -->
         <div id="page-wrapper" style="min-height: 650px">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            <?php
                            $pg = str_replace('_', ' ', $page);
                            $pg = ucwords($pg);
                            echo $pg;
                            ?> <small> Overview</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i> Dashboard
                            </li>
                            <li class="active">
                                <i class="fa fa-table"></i> <?=$pg?>
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class="col-lg-12">
                        <div class="pull-left">
                            <h2>Rencana Kerjana dan Anggaran</h2>
                        </div>
                        <div class="pull-right" style="margin-top: 20px">
                            <!--a href="tambah_buah.php" class="btn btn-info"><i class="fa fa-plus"></i> Tambah Buah</a-->
                            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> Tambah Data</button>
                        </div>
                        <div class="clearfix"></div>
                        <table class="table table-bordered table-hover" style="">
                            <thead>
                                <tr>
                                    <th class="text-center" width="5%">Kode Rekening</th>
                                    <th class="text-center">Uraian</th>
                                    <th width="15%">Harga Satuan</th>
                                    <th width="15%">Jumlah</th>
                                </tr>
                            </thead>
                            <tbody id="bodyData">
                                
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->
            <!-- /.CONTENT -->

        </div>


<!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><i class="fa fa-plus"></i> Tambah Rencana Kerja dan Anggaran</h4>
        </div>
        <div class="modal-body">
          <form id="form_buah" class="form-horizontal" role="form" method="POST">
            <div class="form-group">
                <div class="col-sm-3">
                    <label class="label-control">Kode Rekening: </label>
                </div>
                <div class="col-sm-9">
                    <input type="text" name="kode_rekening" id="kode_rekening" class="form-control" placeholder="Kode Rekening..." autofocus>
                </div>
            </div>
            <div class="form-group">
            <div class="col-sm-3">
                <label class="label-control">Uraian : </label>
            </div>
            <div class="col-sm-9">
                <input type="text" name="uraian" id="uraian" class="form-control" placeholder="Uraian...">
            </div>
            </div>
            <div class="form-group">
            <div class="col-sm-3">
                <label class="label-control">Harga Satuan : </label>
            </div>
            <div class="col-sm-9">
                <input type="text" name="hargasatuan" id="hargasatuan" class="form-control" placeholder="Harga Satuan...">
            </div>
            </div>
            <div class="form-group">
            <div class="col-sm-3">
                <label class="label-control">Jumlah : </label>
            </div>
            <div class="col-sm-9">
                <input type="text" name="jumlah" id="jumlah" class="form-control" placeholder="Jumlah...">
            </div>
            </div>
          <!--/form-->
        </div>
        <div class="modal-footer">
            <button type="button" id="sub_buah" class="btn btn-primary">Simpan</button>
          <button type="button" onclick="this.form.reset();" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </form>
      </div>
      
    </div>
  </div>
</div>
	</div>
    <!-- Modal -->

		<!-- JS SCRIPT -->
		<?php $this->load->view('templates/script'); ?>
		<script type="text/javascript">
			$(document).ready(function () {
				(function getData(){
					$.ajax({
						url: "<?=base_url().$page;?>/get_data",
						success: function(res){
							$("#bodyData").html(res);
                            
						}
					});
				})();
                $("#sub_buah").on('click', function(){
                    $.ajax({
                        url: '<?=base_url()?>rka/adding_data', 
                        type: 'post',
                        data: $("#form_buah").serialize(),
                        success: function(res){
                            $("#uraian").val("");
                            $("#kode_rekening").val("");
                            $("#hargasatuan").val("");
                            $("#jumlah").val("");
                            $('#myModal').modal('toggle');
                            getData();
                        },
                        error: function(res){
                            alert("ID Buah Sudah Ada");
                        }
                    })
                });

                $('#myModalEdit').on('show.bs.modal', function (e) {
                	var id = $(e.relatedTarget).data('id');
                	$.ajax({
                        type: 'POST',
                		url: '<?=base_url()?>rka/edit_data',
                		data: 
                            'rowid='+ id
                        ,
                		success: function(res){
                			
                			$("#modal_edit").html(res);
                		},
                		error: function(res){
                			alert("GAGAL");
                		}
                	})
                });
				function simpan_edit(){

                }
			})
		</script>
		<!-- END JS SCRIPT -->	
</body>
</html>