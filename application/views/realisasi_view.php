<!DOCTYPE html>
<html lang="en">
	<?php $this->load->view('templates/head'); ?>
<body>
    <div id="wrapper">
		<!-- NAVIGATION -->
		<?php $this->load->view('templates/nav'); ?>
		<!-- END NAVIGATION -->

		<!-- CONTENT -->
         <div id="page-wrapper" style="min-height: 650px">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            <?php
                            $pg = str_replace('_', ' ', $page);
                            $pg = ucwords($pg);
                            echo $pg;
                            ?> <small> Overview</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-home"></i> Dashboard
                            </li>
                            <li class="active">
                                <i class="fa fa-table"></i> <?=$pg?>
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class="col-lg-12">
                        <div class="pull-left">
                            <h2>Data Realisasi</h2>
                        </div>
                        <div class="pull-right" style="margin-top: 20px">
                            <!--a href="tambah_buah.php" class="btn btn-info"><i class="fa fa-plus"></i> Tambah Buah</a-->
                            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> Input Realisasi</button>
                        </div>
                        <div class="clearfix"></div>
                        <table class="table table-bordered table-hover" style="">
                            <thead>
                                <tr>
                                    <th class="text-center" nowrap="">Uraian</th>
                                    <th class="text-center" width="10%" nowrap="">Anggaran/Tahun</th>
                                    <th class="text-center" width="15%" nowrap="">Anggaran/Bulan</th>
                                    <th class="text-center" width="30%" nowrap="">Realisasi</th>
                                    <th class="text-center" width="20%" nowrap="">Jumlah</th>
                                </tr>
                            </thead>
                            <tbody id="bodyData">
                                <?php
                                foreach ($data_realisasi as $row) {
                                    ?>
                                    <tr>
                                        <td><?=$row->uraian?></td>
                                        <td class="text-center"><?=$row->anggaran_tahun;?></td>
                                        <td class="text-center"><?=$row->anggaran_bulan;?></td>
                                        <td><?=$row->realisasi;?></td>
                                        <td class="text-right">Rp <?=$row->jumlah;?></td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->
            <!-- /.CONTENT -->

        </div>


<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><i class="fa fa-plus"></i> Input Realisasi</h4>
        </div>
            <div class="modal-body">
                <form id="form_realisasi" class="form-horizontal" role="form" method="POST">
                    <div class="form-group">
                        <div class="col-lg-3">
                            <label class="control-label">Uraian:</label>
                        </div>
                        <div class="col-lg-9">
                            <div class="input-group">
                                <select name="jenis_uraian" class="form-control" id="jenis_uraian">
                                    <option selected="" disabled=""></option>
                                    <?php
                                    foreach ($data_uraian as $key) {
                                        ?>
                                        <option value="<?=$key->kode_rekening?>" label="<?=$key->nama_buah?>"><?=ucwords($key->nama_buah);?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <div class="input-group-btn">
                                  <button class="btn btn-default" data-toggle="modal" data-target="#ModalAddUraian" type="button">
                                    <i class="fa fa-plus-circle" title="Add"></i>
                                  </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group" id="form-sub1">
                        <div class="col-lg-3">
                            <label class="control-label">Sub Bab 1:</label>
                        </div>
                        <div class="col-lg-9" id="input-sub1">
                            
                        </div>
                    </div>
                    <div class="form-group" id="form-sub2">
                        <div class="col-lg-3">
                            <label class="control-label">Sub Bab 2:</label>
                        </div>
                        <div class="col-lg-9" id="input-sub2">
                            
                        </div>
                    </div>
                    <div class="form-group" id="form-sub3">
                        <div class="col-lg-3">
                            <label class="control-label">Sub Bab 3:</label>
                        </div>
                        <div class="col-lg-9" id="input-sub3">
                            
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label class="label-control">Anggaran/Tahun: </label>
                        </div>
                        <div class="col-sm-9">
                            <input type="number" name="angg_tahun" id="angg_tahun" class="form-control" placeholder="" min="2000">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label class="label-control">Anggaran/Bulan: </label>
                        </div>
                        <div class="col-sm-9">
                            <select name="angg_bulan" id="angg_bulan" class="form-control">
                                <option selected="" disabled=""></option>
                                <option value="Januari/Maret">Januari/Maret</option>
                                <option value="April/Juni">April/Juni</option>
                                <option value="Juli/September">Juli/September</option>
                                <option value="Oktober/Desember">Oktober/Desember</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label class="label-control">Realisasi: </label>
                        </div>
                        <div class="col-sm-9" id="realisasi_input">
                            <input readonly="" type="text" name="realisasi" id="realisasi" class="form-control" placeholder="Realisasi...">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label class="label-control">Jumlah: </label>
                        </div>
                        <div class="col-sm-9" id="maxharga">
                            <input readonly="" type="number" name="jumlah" id="jumlah" class="form-control" max="">
                        </div>
                    </div>
                </form>
              <!--/form-->
            </div>
        <div class="modal-footer">
            <button type="button" id="add_realisasi" class="btn btn-primary">Simpan</button>
          <button type="reset" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
</div>

<!-- Modal Add Uraian -->
<div class="modal fade" id="ModalAddUraian" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><i class="fa fa-plus"></i> Tambah Uraian</h4>
        </div>
        <div class="modal-body">
          <form id="form_uraian" class="form-horizontal" role="form" method="POST">
            <div class="form-group">
                <div class="col-sm-3">
                    <label class="label-control">Kode Rekening: </label>
                </div>
                <div class="col-sm-9">
                    <input type="text" name="kode_rekening_main" id="kode_rekening_main" class="form-control" placeholder="Kode Rekening..." autofocus>
                </div>
            </div>
            <div class="form-group">
            <div class="col-sm-3">
                <label class="label-control">Uraian: </label>
            </div>
            <div class="col-sm-9">
                <input type="text" name="nama_uraian_main" id="nama_uraian_main" class="form-control" placeholder="Uraian...">
            </div>
            </div>
          <!--/form-->
        </div>
        <div class="modal-footer">
            <button type="button" id="simpan_main_sub" class="btn btn-primary">Simpan</button>
          <button type="button" onclick="this.form.reset();" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </form>
      </div>
      
    </div>
  </div>
</div>
<!-- END Modal Add Uraian -->


	</div>
		<!-- JS SCRIPT -->
		<?php $this->load->view('templates/script'); ?>
		<script type="text/javascript">
			$(document).ready(function () {
                $("#form-sub1").hide();
                $("#form-sub2").hide();
                $("#form-sub3").hide();
                var no_rek = '';
                var sub = '';
                var subs = '';
                var subss='';
                $("#add_realisasi").on('click', function(){
                    $.post("<?=base_url('Realisasi/insert_realisasi')?>", $("#form_realisasi").serialize(), function(){
                        $("#angg_tahun").val("");
                        $("#angg_bulan").val("");
                        $("#realisasi").val();
                        $("#myModal").modal('toggle');
                        document.location.reload();
                    });
                });
                $("#jenis_uraian").on('change', function(){
                    no_rek = $( this ).val();
                    $.post("<?=base_url('Realisasi/get_input_form1')?>", {kode_rekening: no_rek}, function(res1){
                        $("#form-sub1").show();
                        $("#input-sub1").html(res1);
                        // sub1 change event
                        $("#sub1").on("change", function(){
                            sub = $(this).val();
                            // get sub2
                            $.post('<?=base_url("Realisasi/get_input_form2");?>', {kode_rekening:no_rek, sub:sub}, function(res2){
                                $("#form-sub2").show();
                                $("#input-sub2").html(res2);
                                // sub2 change event
                                $("#sub2").on('change', function(){
                                    subs = $(this).val();
                                    // get sub3
                                    $.post("<?=base_url('Realisasi/get_input_form3');?>", {kode_rekening:no_rek, sub:sub, subs:subs}, function(res3){
                                        $("#form-sub3").show();
                                        $("#input-sub3").html(res3);
                                        $("#sub3").change(function(e){
                                            subss = $(this).val();
                                            $.getJSON("<?=base_url('Realisasi/get_max_harga');?>", {kode_rekening:no_rek, sub:sub, subs:subs, subss:subss}, function(suc){
                                                var htmlharga = '<input type="number" name="jumlah" id="jumlah" class="form-control" value="'+suc.jumlah+'" max="'+suc.jumlah+'" min="1000">';
                                                $("#maxharga").html(htmlharga);
                                                $("#realisasi_input").html('<input readonly="" type="text" name="realisasi" id="realisasi" class="form-control" placeholder="Realisasi..." value="'+suc.nama_sub+'">');
                                            });  
                                        })
                                                                     
                                    })
                                });
                            });
                        });
                    });
                });
                
			});
		</script>
		<!-- END JS SCRIPT -->	
</body>
</html>