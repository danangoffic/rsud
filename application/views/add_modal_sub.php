<!-- Modal -->
          <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">
            
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title"><i class="fa fa-plus"></i> Tambah Uraian</h4>
                </div>
                <div class="modal-body">
                  <form id="form_buah" class="form-horizontal" role="form" method="POST">
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label class="label-control">Kode Rekening: </label>
                        </div>
                        <div class="col-sm-9">
                            <input type="text" name="kode_rekening_main" id="kode_rekening_main" class="form-control" placeholder="Kode Rekening..." autofocus>
                        </div>
                    </div>
                    <div class="form-group">
                    <div class="col-sm-3">
                        <label class="label-control">Uraian: </label>
                    </div>
                    <div class="col-sm-9">
                        <input type="text" name="nama_uraian_main" id="nama_uraian_main" class="form-control" placeholder="Uraian...">
                    </div>
                    </div>
                  <!--/form-->
                </div>
                <div class="modal-footer">
                    <button type="button" id="simpan_main_sub" class="btn btn-primary">Simpan</button>
                  <button type="button" onclick="this.form.reset();" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                </form>
              </div>
              
            </div>
          </div>
        </div>
        <!-- END MODAL -->

        <div class="modal fade" id="modal_add_sub1" role="dialog">
            <div class="modal-dialog">
            
              <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title"><i class="fa fa-plus"></i> Tambah Uraian</h4>
                    </div>
                    <div class="modal-body">
                        <form id="form_sub1" role="form" method="POST">
                            <div class="form-group">
                                <label class="label-control">Nomor Rekening:</label>
                                <input type="text" readonly="" name="no_rekening1" id="no_rekening1" class="form-control" placeholder="Kode Rekening... ">
                            </div>
                            <div class="form-group">
                                <label class="label-control">Uraian Sub:</label>
                                <input autofocus type="text" name="uraian_sub1" id="uraian_sub1" class="form-control" placeholder="Uraian Sub">
                            </div>
                      <!--/form-->
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="save_uraian1" class="btn btn-primary">Simpan</button>
                      <button type="button" onclick="this.form.reset();" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                        </form>
                </div>
            </div>
        </div>
        <!-- END MODAL ADD SUB1 -->

        <!-- MODAL ADD SUB2 -->
        <div class="modal fade" id="modal_add_sub2" role="dialog">
            <div class="modal-dialog">
            
              <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title"><i class="fa fa-plus"></i> Tambah Uraian</h4>
                    </div>
                    <div class="modal-body">
                        <form id="form_sub2" role="form" method="POST">
                            <div class="form-group">
                                <label class="label-control">Nomor Rekening:</label>
                                <input type="text" readonly="" name="no_rekening2" id="no_rekening2" class="form-control" placeholder="Kode Rekening... ">
                            </div>
                            <div class="form-group">
                                <label class="label-control">ID SUB 1:</label>
                                <input type="text" readonly="" name="id_sub1" id="id_sub1" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="label-control">Uraian Sub:</label>
                                <input autofocus type="text" name="uraian_sub2" id="uraian_sub2" class="form-control" placeholder="Uraian Sub">
                            </div>
                      <!--/form-->
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="save_uraian2" class="btn btn-primary">Simpan</button>
                      <button type="button" onclick="this.form.reset();" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                        </form>
                </div>
            </div>
        </div>
        <!-- END MODAL ADD SUB2 -->

        <!-- MODAL ADD SUB3 -->
        <div class="modal fade" id="modal_add_sub3" role="dialog">
            <div class="modal-dialog">
            
              <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title"><i class="fa fa-plus"></i> Tambah Uraian</h4>
                    </div>
                    <div class="modal-body">
                        <form id="form_sub3" role="form" method="POST">
                            <div class="form-group">
                                <label class="label-control">Nomor Rekening:</label>
                                <input type="text" readonly="" name="no_rekening3" id="no_rekening3" class="form-control" placeholder="Kode Rekening... ">
                            </div>
                            <div class="form-group">
                                <label class="label-control">ID SUB 1:</label>
                                <input type="text" readonly="" name="id_sub2" id="id_sub2" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="label-control">ID SUB 2:</label>
                                <input type="text" readonly="" name="id_sub3" id="id_sub3" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="label-control">Uraian Sub:</label>
                                <input autofocus type="text" name="uraian_sub3" id="uraian_sub3" class="form-control" placeholder="Uraian Sub">
                            </div>
                            <div class="form-group">
                                <label class="label-control">Volume:</label>
                                <input type="number" name="volume" id="volume" class="form-control" placeholder="Volume">
                            </div>
                            <div class="form-group">
                                <label class="label-control">Satuan Volume:</label>
                                <input type="text" name="satuan_volume" id="satuan_volume" class="form-control" placeholder="Satuan Volume...">
                            </div>
                            <div class="form-group">
                                <label class="label-control">Harga Satuan:</label>
                                <input type="number" name="hargasatuan" id="hargasatuan" class="form-control" placeholder="Harga Satuan..">
                            </div>
                      <!--/form-->
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="save_uraian3" class="btn btn-primary">Simpan</button>
                      <button type="button" onclick="this.form.reset();" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                        </form>
                </div>
            </div>
        </div>