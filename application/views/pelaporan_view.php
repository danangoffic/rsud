<!DOCTYPE html>
<html lang="en">
	<?php $this->load->view('templates/head'); ?>
<body>
    <div id="wrapper">
		<!-- NAVIGATION -->
		<?php $this->load->view('templates/nav'); ?>
		<!-- END NAVIGATION -->

		<!-- CONTENT -->
         <div id="page-wrapper" style="min-height: 650px">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            <?php
                            $pg = str_replace('_', ' ', $page);
                            $pg = ucwords($pg);
                            echo $pg;
                            ?> Data RKA <small> Overview</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-home"></i> Dashboard
                            </li>
                            <li class="active">
                                <i class="fa fa-tasks"></i> Data RKA
                            </li>
                        </ol>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="pull-left">
                            <h2>Data RKA</h2>
                        </div>
                        <div class="pull-right" style="margin-top: 20px">
                            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> Tambah RKA</button>
                        </div>
                        <div class="clearfix"></div>
                        <table class="table table-bordered table-hover" style="">
                            <thead>
                                <tr>
                                    <th class="text-center" width="12%">Kode Rek</th>
                                    <th class="text-center">Uraian</th>
                                    <th width="25%"></th>
                                </tr>
                            </thead>
                            <tbody id="bodyData">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    
    <!-- /#page-wrapper -->
		<!-- END CONTENT -->
        <div id="myModalEdit" class="modal fade" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content" id="modal_edit">
              
            </div>

          </div>
        </div> 

        <!-- MODAL SUB 1 -->
        <div id="modal_sub" class="modal fade" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="title-subs"></h4>
              </div>
              <div class="modal-body" id="content-subs">
                
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>

          </div>
        </div>
        <!-- END MODAL SUB1 -->

        <!-- MODAL SUB 2 -->
        <div id="modal_sub2" class="modal fade" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="title-subs2"></h4>
              </div>
              <div class="modal-body" id="content-subs2">
                
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>

          </div>
        </div>
        <!-- END MODAL SUB 2 -->

        <!-- MODAL SUB 3 -->
        <div id="modal_sub3" class="modal fade" role="dialog">
          <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="title-subs3"></h4>
              </div>
              <div class="modal-body" id="content-subs3">
                
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>

          </div>
        </div>
        <!-- END MODAL SUB 3 -->

        <!-- ================== ADD MODAL SUB ================== -->
        <!-- MODAL ADD SUB1 -->
        <?php $this->load->view('add_modal_sub'); ?>
        <!-- END MODAL ADD SUB3 -->
        <!-- ================== END ADD SUB MODAL ================== -->


        <!-- =========== EDIT MODAL =========== -->
        <!-- MODAL EDIT MAIN SUB -->
        <!-- Modal -->
        <?php $this->load->view('edit_modal_sub'); ?>
        <!-- END MODAL EDIT SUB3 -->




	</div>
		<!-- JS SCRIPT -->
		<?php $this->load->view('templates/script'); ?>
		<script type="text/javascript">
			$(document).ready(function () {
                 var no_rekening = "";
                 var subs = null;
                 var hargasatuan = null;
                 var jumlah = null;
                 var sub = null;
                 var nama_sub = null;
				(function getData(){
					$.ajax({
						url: "<?=base_url('Pelaporan');?>/get_data_laporan",
						success: function(res){
							$("#bodyData").html(res);
						}
					});
				})();

                // ===========save main sub============= //

                $("#simpan_main_sub").on('click', function(){
                    $.ajax({
                        url: '<?=base_url("Pelaporan")?>/add_main_sub', 
                        type: 'post',
                        data: {nama_uraian_main:$("#nama_uraian_main").val(), kode_rekening_main:$("#kode_rekening_main").val()},
                        success: function(res){
                            $("#nama_uraian_main").val("");
                            $("#kode_rekening_main").val("");
                            $('#myModal').modal('toggle');
                            document.location.reload();
                        },
                        error: function(res){
                            alert("ID Buah Sudah Ada");
                        }
                    })
                });
                // ============= modal sub 1 ===============//
                $("#modal_sub").on('show.bs.modal', function(e){
                    no_rekening = $(e.relatedTarget).data('rekening');
                    // get data sub1
                    $.ajax({
                        type: 'POST',
                        dataType: 'JSON',
                        url: '<?=base_url("Pelaporan/sub_uraian")?>',
                        data:{no_rekening:no_rekening},
                        success: function(result){
                            var n = '<table class="table table-bordered table-hover" style="">'+
                            '<thead>'+
                                '<tr>'+
                                    '<th class="text-center" nowrap>Kode Rek</th>'+
                                    '<th class="text-center" nowrap>Nama Sub</th>'+
                                    '<th width="40%" nowrap></th>'+
                                '</tr>'+
                            '</thead>';
                            var no = 1;
                            $("#title-subs").html(result.title + ' Dengan Nomor Rekening:   ' + result.no_rek);
                            // extract data sub1
                            $.each(result.records, function(i, field){
                                n += '<tbody id="'+field.kode_rekening+field.sub+'">'+
                                        '<tr>'+
                                        '<td class="text-center">'+no_rekening+'.'+no+'</td>'+
                                        '<td class="text-center">'+field.nama_sub.toUpperCase()+'</td>'+
                                        '<td class="text-center" nowrap>'+
                                        '<button type="button" data-no1="'+field.no+'" class="btn btn-success btn-sm" data-rekening="'+field.kode_rekening+'" data-sub='+field.sub+' id="edit_sub1" data-toggle="modal" data-target="#modalEditSub1">Edit</button>'+
                                        '<a type="button" href="'+"<?=base_url("Pelaporan/delete_sub1?no=");?>"+field.no+'" class="btn btn-danger btn-sm" data-rekening="'+field.kode_rekening+'" data-sub='+field.sub+' id="delete_sub1">Delete</a>'+
                                        '<button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#modal_sub2" data-rekening="'+field.kode_rekening+'" data-sub="'+field.sub+'" data-no1="'+field.no+'">Sub Uraian</button></td>'+
                                    '</tr></tbody>';
                                no++;
                            });
                            $("#content-subs").html(n);
                            $("#content-subs").append('<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal_add_sub1" data-rek="'+no_rekening+'"><i class="fa fa-plus"></i> Tambah RKA</button>');
                            
                            // modal sub 2

                        }
                    });
                });

                // ============= MODAL ADD SUB 1 ============= //

                $('#modal_add_sub1').on('show.bs.modal', function(){                    
                    $("#no_rekening1").val(no_rekening);
                    $("#save_uraian1").on('click',function(){
                        var uraian = $("#uraian_sub1");
                        var uraianval = $("#uraian_sub1").val();
                            // save add sub1
                            $.ajax({
                                type: "POST",
                                url: "<?=base_url('Pelaporan/add_sub1');?>",
                                data: {no_rekening:no_rekening, uraian_sub:$("#uraian_sub1").val()},
                                success: function(r){
                                    $("#no_rekening1").val("");
                                    $("#uraian_sub1").val("");
                                    document.location.reload();
                                },
                            });
                    });
                });

                // ================ modal sub 2 ============== //
                $("#modal_sub2").on('show.bs.modal', function(e){
                    sub = $(e.relatedTarget).data('sub');
                    $.getJSON("<?=base_url('Pelaporan/sub_uraian2')?>", {no_rekening:no_rekening, sub:sub}, function(hs){
                        var m = '<table class="table table-bordered table-hover" style="">'+
                                '<thead>'+
                                    '<tr>'+
                                        '<th class="text-center" width="5%">No</th>'+
                                        '<th class="text-center">Nama Sub</th>'+
                                        '<th width="40%"></th>'+
                                    '</tr>'+
                                '</thead>';
                            
                            $("#title-subs2").html(hs.titlemain + ' Pada Sub ' + hs.titlesub + ' Dengan Kode Rekening ' + hs.no_rekening);
                            if(hs.records==""){
                                $("#content-subs2").html('<div><strong>Data Kosong.</strong></div>');
                            }else{
                                var nom = 1;
                                $.each(hs.records, function(i, field){
                                    m += '<tbody id="'+field.kode_rekening+field.sub+'">'+
                                            '<tr>'+
                                            '<td class="text-center">'+field.kode_rekening+'.'+field.sub+'.'+nom+'</td>'+
                                            '<td class="text-center">'+field.nama_sub.toUpperCase()+'</td>'+
                                            '<td class="text-center" nowrap>'+
                                            '<button type="button" data-no2="'+field.no+'" data-toggle="modal" data-target="#modalEditSub2" data-rek2="'+field.no+'" class="btn btn-success btn-sm" data-rekening="'+field.kode_rekening+'" data-sub='+field.sub+' id="edit_sub2">Edit</button>'+
                                            '<a type="button" href="'+"<?=base_url("Pelaporan/delete_sub1?no=");?>"+field.no+'" class="btn btn-danger btn-sm" data-rekening="'+field.kode_rekening+'" data-sub='+field.sub+' id="delete_sub1">Delete</a>'+
                                            '<button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#modal_sub3" data-rekening="'+field.kode_rekening+'" data-sub1="'+field.sub+'" data-subs="'+field.subs+'">Sub Uraian</button></td>'+
                                        '</tr></tbody>';
                                    nom++;
                                });
                                $("#content-subs2").html(m);
                            }
                            $("#content-subs2").append('<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal_add_sub2" data-rek="'+hs.no_rekening+'" data-sub="'+hs.sub+'"><i class="fa fa-plus"></i> Tambah RKA</button>');
                    });
                });

                // ============= MODAL ADD SUB 2 =========== //

                $('#modal_add_sub2').on('show.bs.modal', function(e){
                    $("#id_sub1").val(sub);
                    $("#no_rekening2").val(no_rekening);
                    $("#save_uraian2").on('click',function(){
                        var uraian2 = $("#uraian_sub2");
                        var uraianval2 = $("#uraian_sub2").val();
                            // save add sub1
                            $.ajax({
                                type: "POST",
                                url: "<?=base_url('Pelaporan/add_sub2')?>",
                                data: {no_rekening:$("#no_rekening2").val(), uraian_sub:$("#uraian_sub2").val(), id_sub:sub},
                                success: function(r){
                                    $("#no_rekening2").val("");
                                    $("#uraian_sub2").val("");
                                    $("#id_sub1").val("");
                                    $('#modal_add_sub2').modal('toggle');
                                    document.location.reload();
                                },
                            });
                    });
                });

                // =============== MODAL SUB 3 ============== //

                $("#modal_sub3").on('show.bs.modal', function(e){
                    subs = $(e.relatedTarget).data('subs');
                    $.getJSON("<?=base_url('Pelaporan/sub_uraian3');?>", {no_rekening:no_rekening, sub:sub, subs:subs}, function(hss){
                        var o = '<table class="table table-bordered table-hover" style="">'+
                                '<thead>'+
                                    '<tr>'+
                                        '<th class="text-center" width="5%">Kode Rek</th>'+
                                        '<th class="text-center">Nama Sub</th>'+
                                        '<th class="text-center">Volume</th>'+
                                        '<th class="text-center">Harga Satuan</th>'+
                                        '<th class="text-center">Jumlah Harga</th>'+
                                        '<th class="text-center"></th>'+
                                    '</tr>'+
                                '</thead>';
                            
                            $("#title-subs3").html(hss.titlemain + ' Pada Sub ' + hss.titlesub + ' Dengan Kode Rekening ' + hss.no_rekening);
                            if(hss.records==""){
                                $("#content-subs3").html('<div><strong>Data Kosong.</strong></div>');
                            }else{
                                var nomr = 1;
                                $.each(hss.records, function(i, field){
                                    o += '<tbody id="'+field.kode_rekening+field.sub+'">'+
                                            '<tr>'+
                                            '<td class="text-center">'+field.kode_rekening+'.'+field.sub+'.'+field.subs+'.'+nomr+'</td>'+
                                            '<td class="text-center">'+field.nama_sub.toUpperCase()+'</td>'+
                                            '<td class="text-right">'+field.volume+'</td>'+
                                            '<td class="text-right">'+field.rp+field.harga_satuan+'</td>'+
                                            '<td class="text-right">'+field.rp+field.jumlah+'</td>'+
                                            '<td class="text-center" nowrap>'+
                                            '<button type="button" data-no3="'+field.no+'" data-toggle="modal" data-target="#modalEditSub3" data-rek3="'+field.no+'" class="btn btn-success btn-sm" data-rekening="'+field.kode_rekening+'" data-sub='+field.sub+' id="edit_sub3">Edit</button>'+
                                            '<a type="button" href="'+"<?=base_url("Pelaporan/delete_sub1?no=");?>"+field.no+'" class="btn btn-danger btn-sm" data-rekening="'+field.kode_rekening+'" data-sub='+field.sub+' id="delete_sub1">Delete</a>'+
                                        '</tr></tbody>';
                                    nomr++;
                                });
                                $("#content-subs3").html(o);
                            }
                            $("#content-subs3").append('<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal_add_sub3" data-rek="'+hss.no_rekening+'" data-sub="'+hss.sub+'" data-subs="'+hss.subs+'"><i class="fa fa-plus"></i> Tambah RKA</button>');
                    });
                });

                // ================= MODAL ADD SUB 3 =================== //
                $('#modal_add_sub3').on('show.bs.modal', function(e){
                    $("#id_sub2").val(sub);
                    $("#id_sub3").val(subs);
                    $("#no_rekening3").val(no_rekening);
                    $("#save_uraian3").on('click',function(){
                        var uraian3 = $("#uraian_sub3");
                        var uraianval3 = $("#uraian_sub3").val();
                            // save add sub1
                            $.ajax({
                                type: "POST",
                                url: "<?=base_url('Pelaporan/add_sub3')?>",
                                data: {no_rekening:$("#no_rekening3").val(), uraian_sub:$("#uraian_sub3").val(), sub:sub, subs:subs, hargasatuan:$("#hargasatuan").val(), subs:subs, volume:$("#volume").val(), satuan_volume:$("#satuan_volume").val()},
                                success: function(r){
                                    $("#no_rekening3").val("");
                                    $("#uraian_sub3").val("");
                                    $("#hargasatuan").val("");
                                    $("#satuan_volume").val("")
                                    $("#volume").val("");
                                    $('#modal_add_sub3').modal('toggle');
                                    // document.location.reload();
                                },
                            });
                    });
                });


                $('#modalEditMain').on('show.bs.modal', function (e) {
                	var kode_rekening = $(e.relatedTarget).data('rek');
                	$.ajax({
                        type: 'POST',
                		url: '<?=base_url()?>Pelaporan/getDataMain',
                		data: 
                            {kode_rekening:kode_rekening}
                        ,
                		success: function(res){
                			$("#edit_main_kd").val(kode_rekening);
                			$("#edit_kode_rekening_main").val(res.kode_rekening);
                            $("#edit_nama_uraian_main").val(res.nama_buah);
                            $("#edit_simpan_main_sub").click(function(){
                                console.log($("#form_editmain").serialize());
                                $.ajax({
                                    url: "<?=base_url('Pelaporan/updateMain');?>",
                                    type: 'POST',
                                    data: $("#form_editmain").serialize(),
                                    success: function(e){
                                      
                                    },
                                    error: function(){
                                      alert("ERROR");
                                    }
                                })
                            })
                		},
                		error: function(res){
                			alert("GAGAL");
                		}
                	})
                });

                $("#modalEditSub1").on('show.bs.modal', function(e){
                    var no1 = $(e.relatedTarget).data('no1');
                    $.post("<?=base_url('Pelaporan/getDataSub')?>", {no:no1}, function(s1){
                      $("#no1").val(s1.no);
                      $("#edit_no_rekening1").val(s1.kode_rekening);
                      $("#edit_id_sub1").val(s1.kode_rekening + "." + s1.sub);
                      $("#edit_uraian_sub1").val(s1.nama_sub);
                      $("#save_edituraian1").click(function(s1act){
                        $.post("<?=base_url('Pelaporan/updateDataSub')?>", $("#form_editsub1").serialize(), function(e){
                          if(e=="true"){
                            $("#alert_1").html('<div class="alert alert-success fade in alert-dismissible">'+
                                                '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+
                                                '<strong>Success!</strong> Berhasil meng-update data.'+
                                              '</div>');
                            $("#alert").fadeTo(2000, 500).slideUp(500, function(){
                              $("#alert").slideUp(500);
                            });
                          }else{
                            $("#alert_1").html('<div class="alert alert-danger fade in alert-dismissible">'+
                                                '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+
                                                '<strong>Warning!</strong> Gagal meng-update data.'+
                                              '</div>');
                            $("#alert").fadeTo(2000, 500).slideUp(500, function(){
                              $("#alert").slideUp(500);
                            });
                          }
                        })
                      })
                    });
                });
                $("#modalEditSub2").on('show.bs.modal', function(e){
                    var no2 = $(e.relatedTarget).data('no2');
                    $.post("<?=base_url('Pelaporan/getDataSub')?>", {no:no2}, function(s2){
                      $("#no2").val(s2.no);
                      $("#edit_no_rekening2").val(s2.kode_rekening);
                      $("#edit_id_sub2a").val(s2.kode_rekening + "." + s2.sub);
                      $("#edit_id_sub2b").val(s2.kode_rekening + "." + s2.sub + "." + s2.subs);
                      $("#edit_uraian_sub2").val(s2.nama_sub);
                      $("#save_edituraian2").click(function(s2act){
                        $.post("<?=base_url('Pelaporan/updateDataSub2')?>", $("#form_editsub2").serialize(), function(e){
                          if(e==""){
                            $("#alert_2").html('<div class="alert alert-success fade in alert-dismissible">'+
                                                '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+
                                                '<strong>Success!</strong> Berhasil meng-update data.'+
                                              '</div>');
                            $("#alert").fadeTo(2000, 500).slideUp(500, function(){
                              $("#alert").slideUp(500);
                            });

                          }else{
                            $("#alert_2").html('<div class="alert alert-danger fade in alert-dismissible">'+
                                                '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+
                                                '<strong>Warning!</strong> Gagal meng-update data.'+
                                              '</div>');
                            $("#alert").fadeTo(2000, 500).slideUp(500, function(){
                              $("#alert").slideUp(500);
                            });
                          }
                        });
                      });
                    });
                })
                $("#modalEditSub3").on('show.bs.modal', function(e){
                    var no3 = $(e.relatedTarget).data('no3');
                    $.post("<?=base_url('Pelaporan/getDataSub')?>", {no:no3}, function(s3){
                      $("#no3").val(s3.no);
                      $("#no_rekening3").val(s3.kode_rekening);
                      $("#id_sub3a").val(s3.kode_rekening + "." + s3.sub);
                      $("#id_sub3b").val(s3.kode_rekening + "." + s3.sub + "." + s3.subs);
                      $("#id_sub3c").val(s3.kode_rekening + "." + s3.sub + "." + s3.subs + "." + s3.subss);
                      $("#uraian_sub3a").val(s3.nama_sub);
                      $("#volume3").val(s3.volume);
                      $("#edit_satuan_volume").val(s3.satuan);
                      $("#hargasatuan3").val(s3.harga_satuan);
                      $("#save_edituraian3").click(function(s3act){
                        $.post("<?=base_url('Pelaporan/updateDataSub3');?>", $("#form_editsub3").serialize(), function(e){
                          if(e==""){
                            $("#alert_3").html('<div class="alert alert-success fade in alert-dismissible">'+
                                                '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+
                                                '<strong>Success!</strong> Berhasil meng-update data.'+
                                              '</div>');
                            $("#alert").fadeTo(2000, 500).slideUp(500, function(){
                              $("#alert").slideUp(500);
                            });
                          }else{
                            $("#alert_3").html('<div class="alert alert-success fade in alert-dismissible">'+
                                                '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+
                                                '<strong>Success!</strong> Berhasil meng-update data.'+
                                              '</div>');
                            $("#alert").fadeTo(2000, 500).slideUp(500, function(){
                              $("#alert").slideUp(500);
                            });
                          }
                        })
                      })
                    });
                })
				
			})
		</script>
		<!-- END JS SCRIPT -->	
</body>
</html>