<div class="modal fade" id="modalEditMain" role="dialog">
            <div class="modal-dialog">
            
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title"><i class="fa fa-plus"></i> Tambah Uraian</h4>
                </div>
                <div class="modal-body">
                  <form id="form_editmain" class="form-horizontal" role="form" method="POST">
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label class="label-control">Kode Rekening: </label>
                        </div>
                        <div class="col-sm-9">
                            <input type="hidden" name="edit_main_kd" id="edit_main_kd" class="form-control">
                            <input type="text" name="edit_kode_rekening_main" id="edit_kode_rekening_main" class="form-control" placeholder="Kode Rekening...">
                        </div>
                    </div>
                    <div class="form-group">
                    <div class="col-sm-3">
                        <label class="label-control">Uraian: </label>
                    </div>
                    <div class="col-sm-9">
                        <input type="text" name="edit_nama_uraian_main" id="edit_nama_uraian_main" class="form-control" placeholder="Uraian...">
                    </div>
                    </div>
                  <!--/form-->
                </div>
                <div class="modal-footer">
                    <button type="button" id="edit_simpan_main_sub" class="btn btn-primary">Simpan</button>
                  <button type="button" onclick="this.form.reset();" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                </form>
              </div>
              
            </div>
          </div>
        </div>
        <!-- END MODAL -->
        <!-- END MODAL EDIT MAIN SUB -->

        <!-- MODAL EDIT SUB1 -->
        <div class="modal fade" id="modalEditSub1" role="dialog">
            <div class="modal-dialog">
            
              <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title"><i class="fa fa-plus"></i> Tambah Uraian</h4>
                    </div>
                    <div class="modal-body">
                        <form id="form_editsub1" role="form" method="POST">
                            <div class="form-group">
                                <label class="label-control">Nomor Rekening:</label>
                                <input type="hidden" name="no1" id="no1">
                                <input type="text" readonly="" name="edit_no_rekening1" id="edit_no_rekening1" class="form-control" placeholder="Kode Rekening... ">
                            </div>
                            <div class="form-group">
                                <label class="label-control">ID SUB 1:</label>
                                <input type="text" readonly="" name="edit_id_sub1" id="edit_id_sub1" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="label-control">Uraian Sub:</label>
                                <input autofocus type="text" name="edit_uraian_sub1" id="edit_uraian_sub1" class="form-control" placeholder="Uraian Sub">
                            </div>
                            <div class="form-group" id="alert_1">
                                
                            </div>
                      <!--/form-->
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="save_edituraian1" class="btn btn-primary">Simpan</button>
                      <button type="button" onclick="this.form.reset();" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                        </form>
                </div>
            </div>
        </div>
        <!-- END MODAL EDIT SUB1 -->

        <!-- MODAL EDIT SUB2 -->
        <div class="modal fade" id="modalEditSub2" role="dialog">
            <div class="modal-dialog">
            
              <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title"><i class="fa fa-plus"></i> Tambah Uraian</h4>
                    </div>
                    <div class="modal-body">
                        <form id="form_editsub2" role="form" method="POST">
                            <div class="form-group">
                                <label class="label-control">Nomor Rekening:</label>
                                <input type="hidden" name="no2" id="no2">
                                <input type="text" readonly="" name="edit_no_rekening2" id="edit_no_rekening2" class="form-control" placeholder="Kode Rekening... ">
                            </div>
                            <div class="form-group">
                                <label class="label-control">ID SUB 1:</label>
                                <input type="text" readonly="" name="edit_id_sub2a" id="edit_id_sub2a" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="label-control">ID SUB 2:</label>
                                <input type="text" readonly="" name="edit_id_sub2b" id="edit_id_sub2b" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="label-control">Uraian Sub:</label>
                                <input autofocus type="text" name="edit_uraian_sub2" id="edit_uraian_sub2" class="form-control" placeholder="Uraian Sub">
                            </div>
                            <div class="form-group" id="alert_2">
                                
                            </div>
                      <!--/form-->
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="save_edituraian2" class="btn btn-primary">Simpan</button>
                      <button type="button" onclick="this.form.reset();" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                        </form>
                </div>
            </div>
        </div>
        <!-- END MODAL EDIT SUB2 -->

        <!-- MODAL EDIT SUB3 -->
        <div class="modal fade" id="modalEditSub3" role="dialog">
            <div class="modal-dialog">
            
              <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title"><i class="fa fa-plus"></i> Tambah Uraian</h4>
                    </div>
                    <div class="modal-body">
                        <form id="form_editsub3" role="form" method="POST">
                            <div class="form-group">
                                <input type="hidden" name="no3" id="no3">
                                <label class="label-control">Nomor Rekening:</label>
                                <input type="text" readonly="" name="no_rekening3" id="no_rekening3" class="form-control" placeholder="Kode Rekening... ">
                            </div>
                            <div class="form-group">
                                <label class="label-control">ID SUB 1:</label>
                                <input type="text" readonly="" name="id_sub3a" id="id_sub3a" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="label-control">ID SUB 2:</label>
                                <input type="text" readonly="" name="id_sub3b" id="id_sub3b" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="label-control">ID SUB 3:</label>
                                <input type="text" readonly="" name="id_sub3c" id="id_sub3c" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="label-control">Uraian Sub:</label>
                                <input autofocus type="text" name="uraian_sub3" id="uraian_sub3a" class="form-control" placeholder="Uraian Sub">
                            </div>
                            <div class="form-group">
                                <label class="label-control">Volume:</label>
                                <input type="number" name="volume3" id="volume3" class="form-control" placeholder="Volume">
                            </div>
                            <div class="form-group">
                                <label class="label-control">Satuan Volume:</label>
                                <input type="text" name="edit_satuan_volume" id="edit_satuan_volume" class="form-control" placeholder="Satuan Volume...">
                            </div>
                            <div class="form-group">
                                <label class="label-control">Harga Satuan:</label>
                                <input type="number" name="hargasatuan3" id="hargasatuan3" class="form-control" placeholder="Harga Satuan..">
                            </div>
                            <div class="form-group" id="alert_3">
                                
                            </div>
                      <!--/form-->
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="save_edituraian3" class="btn btn-primary">Simpan</button>
                      <button type="button" onclick="this.form.reset();" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                        </form>
                </div>
            </div>
        </div>