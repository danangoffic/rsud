 <div class="collapse navbar-collapse navbar-ex1-collapse">
    <ul class="nav navbar-nav side-nav">
        <li class="side-user">
            <a class="text-center">
            <img class="img-rounded" src="<?=base_url('library/kebumen.jpg')?>" alt="" width="100">
            <br>SISTEM DAN APLIKASI PELAPORAN<br>RSUD PREMBUN</a>
        </li>
        <li <?=active('dashboard', $page)?>>
            <a href="<?=base_url()?>"><i class="fa fa-fw fa-home"></i> Halaman Utama</a>
        </li>
        <!-- <li <?=active('rka', $page)?>>
            <a href="<?=base_url('Rka')?>"><i class="fa fa-fw fa-table"></i> RKA</a>
        </li> -->
        <li <?=active('pelaporan', $page)?>>
            <a href="<?=base_url('Pelaporan')?>"><i class="fa fa-fw fa-tasks"></i> Data Pelaporan RKA</a>
        </li>
        <li <?=active('realisasi', $page)?>>
            <a href="<?=base_url('Realisasi')?>"><i class="fa fa-fw fa-th-list"></i> Data Realisasi RKA</a>
        </li>
        <li <?=active('managelaporan', $page)?>>
            <a href="<?=base_url('Managelaporan')?>"><i class="fa fa-fw fa-file-contract"></i> Manage Laporan</a>
        </li>
        <li <?=active('manageUsers', $page)?>>
            <a href="<?=base_url('ManageUsers')?>"><i class="fa fa-fw fa-users-cog"></i> Manage Users</a>
        </li>
        <li <?=active('ubahPassword', $page)?>>
            <a href="<?=base_url('UbahPassword');?>"><i class="fa fa-fw fa-lock"></i> Ubah Password</a>
        </li>
    </ul>
</div>
<!-- /.navbar-collapse -->