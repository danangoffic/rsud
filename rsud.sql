-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 22, 2018 at 03:53 AM
-- Server version: 5.6.24
-- PHP Version: 5.5.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `rsud`
--

-- --------------------------------------------------------

--
-- Table structure for table `data_realisasi`
--

CREATE TABLE IF NOT EXISTS `data_realisasi` (
  `no` int(11) NOT NULL,
  `kode_rekening` varchar(30) NOT NULL,
  `anggaran_tahun` year(4) NOT NULL,
  `anggaran_bulan` varchar(20) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `realisasi` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_realisasi`
--

INSERT INTO `data_realisasi` (`no`, `kode_rekening`, `anggaran_tahun`, `anggaran_bulan`, `jumlah`, `realisasi`) VALUES
(3, '5.2.2', 2018, 'Januari/Maret', 49510000, 'A');

-- --------------------------------------------------------

--
-- Table structure for table `data_rka_laporan`
--

CREATE TABLE IF NOT EXISTS `data_rka_laporan` (
  `kode_rekening` varchar(10) NOT NULL,
  `nama_buah` varchar(30) NOT NULL,
  `username` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_rka_laporan`
--

INSERT INTO `data_rka_laporan` (`kode_rekening`, `nama_buah`, `username`) VALUES
('12', 'Belanja Modal', 'admin'),
('123', 'Belanja Pegawai', 'admin'),
('5.2.2', 'Belanja Barang dan Jasa', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `rka`
--

CREATE TABLE IF NOT EXISTS `rka` (
  `no_rek` int(10) NOT NULL,
  `uraian` varchar(50) NOT NULL,
  `hargasatuan` int(10) NOT NULL,
  `jumlah` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rka`
--

INSERT INTO `rka` (`no_rek`, `uraian`, `hargasatuan`, `jumlah`) VALUES
(123, 'Belanja Pegawai', 396923000, 313532000);

-- --------------------------------------------------------

--
-- Table structure for table `sub_uraian`
--

CREATE TABLE IF NOT EXISTS `sub_uraian` (
  `no` int(11) NOT NULL,
  `kode_rekening` varchar(10) NOT NULL,
  `nama_sub` varchar(40) NOT NULL,
  `sub` int(11) DEFAULT NULL,
  `subs` int(11) DEFAULT NULL,
  `subss` int(11) DEFAULT NULL,
  `volume` int(11) DEFAULT NULL,
  `harga_satuan` int(11) DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_uraian`
--

INSERT INTO `sub_uraian` (`no`, `kode_rekening`, `nama_sub`, `sub`, `subs`, `subss`, `volume`, `harga_satuan`, `jumlah`) VALUES
(43, '123', 'uraian 1', 1, NULL, NULL, NULL, NULL, NULL),
(44, '123', 'uraian 2', 2, NULL, NULL, NULL, NULL, NULL),
(45, '12', 'modal 1', 1, NULL, NULL, NULL, NULL, NULL),
(46, '5.2.2', 'Belanja Cetak dan Penggandaan', 1, NULL, NULL, NULL, NULL, NULL),
(50, '5.2.2', 'Belanja Cetak', 1, 1, NULL, NULL, NULL, NULL),
(56, '5.2.2', 'Belanja Cetak Sticker', 1, 1, 1, 1, 20000000, 20000000),
(60, '5.2.2', 'Belanja Cetak Blangko lainnya', 1, 1, 3, 1, 50000000, 50000000);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id_users` varchar(10) NOT NULL,
  `username` varchar(40) NOT NULL,
  `password` varchar(100) NOT NULL,
  `last_login` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id_users`, `username`, `password`, `last_login`) VALUES
('admin', 'admin', '€9$ó7Rö}', '2018-09-14 20:29:41'),
('admin', 'admin2', '€ºÓ U8þ:', '2018-09-14 20:29:41'),
('kasubbag', 'kasubbag', '€FRÉR\\L•lÌÿ"qœ', '2018-09-09 00:56:44'),
('kasubbag', 'pimpinan2', '€œ)Ùâ—Èl\r', '2018-09-09 00:56:44');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `data_realisasi`
--
ALTER TABLE `data_realisasi`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `data_rka_laporan`
--
ALTER TABLE `data_rka_laporan`
  ADD PRIMARY KEY (`kode_rekening`), ADD KEY `username` (`username`);

--
-- Indexes for table `rka`
--
ALTER TABLE `rka`
  ADD PRIMARY KEY (`no_rek`);

--
-- Indexes for table `sub_uraian`
--
ALTER TABLE `sub_uraian`
  ADD PRIMARY KEY (`no`), ADD KEY `kode_rekening` (`kode_rekening`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `data_realisasi`
--
ALTER TABLE `data_realisasi`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `sub_uraian`
--
ALTER TABLE `sub_uraian`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=61;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `data_rka_laporan`
--
ALTER TABLE `data_rka_laporan`
ADD CONSTRAINT `data_rka_laporan_ibfk_1` FOREIGN KEY (`username`) REFERENCES `users` (`username`);

--
-- Constraints for table `sub_uraian`
--
ALTER TABLE `sub_uraian`
ADD CONSTRAINT `sub_uraian_ibfk_1` FOREIGN KEY (`kode_rekening`) REFERENCES `data_rka_laporan` (`kode_rekening`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
