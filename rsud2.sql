-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 23 Sep 2018 pada 19.55
-- Versi Server: 5.6.24
-- PHP Version: 5.5.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `rsud`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_realisasi`
--

CREATE TABLE IF NOT EXISTS `data_realisasi` (
  `no` int(11) NOT NULL,
  `kode_rekening` varchar(30) NOT NULL,
  `no_sub` int(11) NOT NULL,
  `anggaran_tahun` year(4) NOT NULL,
  `anggaran_bulan` varchar(20) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `realisasi` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `data_realisasi`
--

INSERT INTO `data_realisasi` (`no`, `kode_rekening`, `no_sub`, `anggaran_tahun`, `anggaran_bulan`, `jumlah`, `realisasi`) VALUES
(5, '5.2.2', 60, 2018, 'Oktober/Desember', 25000000, 'Triwulan 4'),
(6, '5.2.2', 56, 2018, 'Januari/Maret', 25000000, 'Triwulan 1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_rka_laporan`
--

CREATE TABLE IF NOT EXISTS `data_rka_laporan` (
  `kode_rekening` varchar(10) NOT NULL,
  `nama_buah` varchar(30) NOT NULL,
  `username` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `data_rka_laporan`
--

INSERT INTO `data_rka_laporan` (`kode_rekening`, `nama_buah`, `username`) VALUES
('12', 'Belanja Modal', 'admin'),
('123', 'Belanja Pegawai', 'admin'),
('5.2.2', 'Belanja Barang dan Jasa', 'admin');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sub_uraian`
--

CREATE TABLE IF NOT EXISTS `sub_uraian` (
  `no` int(11) NOT NULL,
  `kode_rekening` varchar(10) NOT NULL,
  `nama_sub` varchar(40) NOT NULL,
  `sub` int(11) DEFAULT NULL,
  `subs` int(11) DEFAULT NULL,
  `subss` int(11) DEFAULT NULL,
  `volume` int(11) DEFAULT NULL,
  `satuan` varchar(10) DEFAULT NULL,
  `harga_satuan` int(11) DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `sub_uraian`
--

INSERT INTO `sub_uraian` (`no`, `kode_rekening`, `nama_sub`, `sub`, `subs`, `subss`, `volume`, `satuan`, `harga_satuan`, `jumlah`) VALUES
(43, '123', 'uraian 1', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(44, '123', 'uraian 2', 2, NULL, NULL, NULL, NULL, NULL, NULL),
(45, '12', 'modal 1', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(46, '5.2.2', 'Belanja Cetak dan Penggandaan', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(50, '5.2.2', 'Belanja Cetak', 1, 1, NULL, NULL, NULL, NULL, NULL),
(56, '5.2.2', 'Belanja Cetak Sticker', 1, 1, 1, 1, 'ls', 20000000, 20000000),
(60, '5.2.2', 'Belanja Cetak Blangko lainnya', 1, 1, 3, 1, 'ls', 50000000, 50000000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id_users` varchar(10) NOT NULL,
  `username` varchar(40) NOT NULL,
  `password` varchar(100) NOT NULL,
  `last_login` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id_users`, `username`, `password`, `last_login`) VALUES
('admin', 'admin', '€9$ó7Rö}', '2018-09-14 20:29:41'),
('admin', 'admin2', '€ºÓ U8þ:', '2018-09-14 20:29:41'),
('kasubbag', 'kasubbag', '€FRÉR\\L•lÌÿ"qœ', '2018-09-09 00:56:44'),
('kasubbag', 'pimpinan2', '€œ)Ùâ—Èl\r', '2018-09-09 00:56:44');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `data_realisasi`
--
ALTER TABLE `data_realisasi`
  ADD PRIMARY KEY (`no`), ADD KEY `kode_rekening` (`kode_rekening`), ADD KEY `no_sub` (`no_sub`);

--
-- Indexes for table `data_rka_laporan`
--
ALTER TABLE `data_rka_laporan`
  ADD PRIMARY KEY (`kode_rekening`), ADD KEY `username` (`username`);

--
-- Indexes for table `sub_uraian`
--
ALTER TABLE `sub_uraian`
  ADD PRIMARY KEY (`no`), ADD KEY `kode_rekening` (`kode_rekening`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `data_realisasi`
--
ALTER TABLE `data_realisasi`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `sub_uraian`
--
ALTER TABLE `sub_uraian`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=61;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `data_realisasi`
--
ALTER TABLE `data_realisasi`
ADD CONSTRAINT `data_realisasi_ibfk_1` FOREIGN KEY (`kode_rekening`) REFERENCES `data_rka_laporan` (`kode_rekening`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `data_realisasi_ibfk_2` FOREIGN KEY (`no_sub`) REFERENCES `sub_uraian` (`no`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `data_rka_laporan`
--
ALTER TABLE `data_rka_laporan`
ADD CONSTRAINT `data_rka_laporan_ibfk_1` FOREIGN KEY (`username`) REFERENCES `users` (`username`);

--
-- Ketidakleluasaan untuk tabel `sub_uraian`
--
ALTER TABLE `sub_uraian`
ADD CONSTRAINT `sub_uraian_ibfk_1` FOREIGN KEY (`kode_rekening`) REFERENCES `data_rka_laporan` (`kode_rekening`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
